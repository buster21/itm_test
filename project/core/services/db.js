const   Sequelize = require('sequelize'),
        config = require('../../config.json');

const Connection = new Sequelize(config.db);

Connection.authenticate().then(()=>console.log("Ok") ).catch(err => { throw err; } );

module.exports.connection = Connection;
module.exports.sequelize = Sequelize;