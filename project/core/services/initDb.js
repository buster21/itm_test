const {connection} = require('./db'),
    user = require('../models/user'),
    exchange = require('../models/exchange');

connection.sync({ force: false })
    .then(() => console.log(`ok`)).catch((err)=>console.log(err));

//exchange.belongsTo(user,{foreignKey: 'out_user_id', sourceKey: 'id'});
//exchange.belongsTo(user,{foreignKey: 'to_user_id', sourceKey: 'id'});

//exchange.hasMany(exchange,{foreignKey:'user_id',sourceKey:'user_id'});
user.hasMany(exchange,{foreignKey: 'to_user_id', as: 'Outgoing'});
user.hasMany(exchange,{foreignKey: 'out_user_id', as: 'Incoming'});
//exchange.belongsTo(user);

/*image.hasMany(Comment, {
    foreignKey: 'commentableId',
    constraints: false,
    scope: {
        commentable: 'image'
    }
});*/

//exchange.hasOne(image,{foreignKey: 'id', sourceKey: 'image_id'});