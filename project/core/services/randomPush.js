const {connection,sequelize} = require("../../core/services/db"),
    exchange = require("../../core/models/exchange"),
    Image = require("../../core/models/image");

const push = function () {
    return new Promise((resolve, reject) => {
        imagesReturn = [];

        // есть ли очередь на отправку
        exchange.findAll({
            where: {
                to_user_id: {
                    [sequelize.Op.is]: null
                }
            },
            order: sequelize.col('createdAt')
        }).then(queueSender => {

            // получить пользователей кому отправить
            // выбираем тех у кого count(out) > count(in)
            exchange.findAll({
                attributes: [
                    'out_user_id',
                    [sequelize.fn('COUNT', '*'), 'out'],
                    [sequelize.literal('IFNULL((SELECT count(*) FROM exchanges e2 WHERE e2.to_user_id=exchange.out_user_id GROUP BY e2.to_user_id),0)'), 'in']
                ],
                having: sequelize.where(sequelize.col('out'), {
                    [sequelize.Op.gt]: sequelize.col('in')
                }),
                group: [sequelize.col('out_user_id')]
            }).then(receivers => {

                receiversArray = receivers ? receivers.map(value => value.dataValues) : null;
                if (typeof receiversArray === null) new Error("No targets");
                return receiversArray;

            }).then(receiversArray => {

                // проход по очереди на отправку
                for (let sender of queueSender) {
                    // фильтруем id отправителя != id получателя и количество исх > вх
                    let byFilter = receiversArray.filter(receiver =>
                        (sender.dataValues.out_user_id !== receiver.out_user_id) && (receiver.out > receiver.in)
                        ),
                        receiverRandom = byFilter.length > 1 ? byFilter[Math.floor(Math.random() * byFilter.length)] : byFilter.shift();

                    if (typeof receiverRandom === "object") {
                        sender.update({
                            "to_user_id": receiverRandom.out_user_id
                        });

                        imagesReturn.push({
                            'to_user_id': receiverRandom.out_user_id,
                            'image': new Image(receiverRandom.out_user_id, sender)
                        });

                        // увелич кол-во вх и сравниваем, если вх больше исх удаляем пользователя из массива получателей
                        if (receiverRandom.out <= ++receiverRandom.in)
                            receiversArray.slice(receiversArray.indexOf(receiverRandom), 1);
                    }
                }

                resolve( imagesReturn );
            });
        }).catch( e => console.log("=(") );
    });
};

module.exports = push;