const Users = require('../models/user'),
    passport = require('passport'),
    LocalStrategy = require('passport-local');

passport.use(new LocalStrategy({
    usernameField: 'user[login]',
    passwordField: 'user[password]',
}, (username, password, done) => {
    Users.findOne({ where:{login:username} })
        .then((user) => {
            if(!user || !user.validatePassword(password)) {
                return done(null, false, { errors: { 'login or password': 'is invalid' } });
            }
            return done(null, user);
        }).catch(done);
}));