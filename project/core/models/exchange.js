const {connection,sequelize} = require('../services/db');

class Exchange extends sequelize.Model {
    static INCOMING = 1;
    static OUTGOING = 0;
}
Exchange.init({
    out_user_id: {
        type: sequelize.INTEGER.UNSIGNED ,
        allowNull: false,
    },
    to_user_id: {
        type: sequelize.INTEGER.UNSIGNED ,
        allowNull: true,
    },
    image: {
        type: sequelize.STRING(150),
        allowNull: false,
    },
}, {
    sequelize: connection,
    modelName: 'exchange',
    indexes: [
        {
            unique: false,
            fields: ['out_user_id','to_user_id']
        },
    ]
});

//Exchange.hasOne(require('./image'),{foreignKey: 'image_id', sourceKey: 'id'});

module.exports = Exchange;