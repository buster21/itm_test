const {connection, sequelize} = require('../services/db'),
      crypt = require('crypto'),
      config = require('../../config'),
      jwt = require('jsonwebtoken');

class User extends sequelize.Model {
    setPassword = (password) => {
        this.password_salt = crypt.randomBytes(16).toString('hex');
        this.password_hash = this._getCrypt(password);
    };

    validatePassword = (password) => this.password_hash === this._getCrypt(password);

    getJWT = () => {
        const today = new Date();
        const expirationDate = new Date(today);
        expirationDate.setDate(today.getDate() + 60);
        return jwt.sign({
            id: this.id,
            exp: parseInt(expirationDate.getTime() / 1000, 10),
        },config.secretJWT);
    };

    authJSON = () => {
        return {
            id: this.id,
            token: this.getJWT(),
        };
    };

    _getCrypt = (password) => {
        if( typeof password !== "string" ) password = password.toString();
        return crypt.pbkdf2Sync(password, this.password_salt, 10000, 256, 'sha256').toString('hex');
    }
}

User.init({
    id: {
        type: sequelize.INTEGER.UNSIGNED ,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    login: {
        type: sequelize.STRING(50),
        allowNull: false,
    },
    password_hash: {
        type: sequelize.STRING(512),
        allowNull: false
    },
    password_salt: {
        type: sequelize.STRING,
        allowNull: false
    }
}, {
    sequelize: connection,
    modelName: 'user',
    indexes: [
        {
            unique: true,
            fields: ['login']
        }
    ]
});

module.exports = User;