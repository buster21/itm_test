const Exchange = require("../../core/models/exchange");

class Image {
    constructor(currentUserId, exchange){
        this.type = currentUserId === exchange.out_user_id ? Exchange.OUTGOING : Exchange.INCOMING;
        this.image = exchange.out_user_id +'/'+ exchange.image;
        this.thumb = 'thumbs/'+exchange.out_user_id +'/'+ exchange.image;
        this.createdAt =  this.type === Exchange.OUTGOING ? exchange.createdAt:exchange.updatedAt;
    }
}

module.exports = Image;