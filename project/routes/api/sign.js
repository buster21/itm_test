const   express = require('express'),
        UserModel = require('../../core/models/user'),
        auth = require('../auth'),
        passport = require('passport');

let router = express.Router();

/*
* SIGN
* */
router.post('/', auth.optional, (req, res, next) => {
    const { body: { user } } = req;

    if( !user ){
        return res.status(400).json({
            error: "bad request",
        });
    }

    if(!user.login || !user.password || !user.rePassword) {
        return res.status(422).json({
            errors: {
                login: 'is required',
                password: 'is required',
                rePassword : 'is required'
            },
        });
    }

    UserModel.findOne({ where:{login:user.login} })
        .then(find =>{
            if( find )
                return res.status(422).json({
                    errors: {
                        login: 'User is exist'
                    },
                });

            if( user.password !== user.rePassword ){
                return res.status(422).json({
                    errors: {
                        password: 'passwords is not equal'
                    },
                });
            }

            const finalUser = new UserModel(user);
            finalUser.setPassword(user.password);

            return finalUser.save().then(() => res.json({ user: finalUser.authJSON() }));
        });
});

/*
* LOGIN
* */
router.post('/login', auth.optional, (req, res, next) => {
    const { body: { user } } = req;

    if(!user || !user.login || !user.password) {
        return res.status(422).json({
            errors: {
                login: 'is required',
                password: 'is required'
            },
        });
    }

    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
        if(err) {
            return next(err);
        }

        if(passportUser) {
            const user = passportUser;
            user.token = passportUser.getJWT();
            return res.json({ user: user.authJSON() });
        }
        return res.status(422).json({error:"Login or Password is incorrect" });
    })(req, res, next);
});

/*
* CURRENT USER
* */
router.get('/current', auth.required, (req, res, next) => {
    const { payload: { id } } = req;
    return UserModel.findByPk(id)
        .then((user) => {
            if(!user) {
                return res.sendStatus(400);
            }

            return res.json({ user: user.authJSON() });
        });
});

module.exports = router;