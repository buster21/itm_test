const express = require('express');
let router = express.Router();

router.use('/sign', require('./sign'));
router.use('/tapes', require('./tapes'));

module.exports = router;