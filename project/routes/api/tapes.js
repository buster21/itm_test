const express = require('express'),
    auth = require('../auth'),
    multer  = require("multer"),
    Image = require("../../core/models/image"),
    {connection,sequelize} = require("../../core/services/db"),
    exchange = require("../../core/models/exchange"),
    thumb = require('node-thumbnail').thumb;

let router = express.Router();

/* GET outgoing images. */
router.get('/', auth.required, function(req, res, next) {
    const { payload: { id } } = req;

    exchange.findAll({
        where: {[sequelize.Op.or]:
            {
                to_user_id:id,
                out_user_id:id
            }
        },
        order: [
            ['updatedAt', 'DESC'],
            ['createdAt', 'DESC']
        ]
    }).then( exchanges => res.json( exchanges.map(value=>new Image(id, value.dataValues)) ) );
});

/* POST image file */
router.post('/upload', auth.required, function(req, res, next) {
    const { payload: { id } } = req,
        fs = require('fs');

    // фильтр
    const fileFilter = (req, file, cb) => {

        if(file.mimetype === "image/png" ||
            file.mimetype === "image/jpg"||
            file.mimetype === "image/jpeg"){
            cb(null, true);
        }
        else{
            cb(null, false);
        }
    };

    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, __dirname+'/../../uploads/'+id)
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now()+'.'+ file.originalname.substr(file.originalname.lastIndexOf('.')+1, file.originalname.length))
        }
    });

    connection.transaction().then(t => {
        let upload = multer({storage: storage,fileFilter: fileFilter }).single('image'),
            folderFull = __dirname+'/../../uploads/'+id,
            folderThumb = __dirname+'/../../uploads/thumbs/'+id;

        [__dirname+'/../../uploads/thumbs',folderThumb,folderFull].forEach(dirs=>!fs.existsSync(dirs)?fs.mkdirSync(dirs):null);

        upload(req, res, (err) => {
            if(!req.file) return res.status(422).json({"error":"error upload file"});

            thumb({
                source: req.file.path,
                destination: folderThumb,
                concurrency: 4,
                prefix: '',
                suffix: '',
                width: 200,
            }).catch(function(e) {
                fs.unlink(req.file.path);
                res.json({"error":"error upload file"});
                throw err;
            }).then(()=>{
                return exchange.create({
                    out_user_id: id,
                    image: req.file.filename,
                    to_user_id:null
                }).then(image=>{
                    res.json(new Image(id,image.dataValues));
                    t.commit();

                    // add event push-images
                    res.app.emit('push-images');
                }).catch();
            });
        });
    });
});

module.exports = router;