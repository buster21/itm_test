let express = require('express'),
    path = require('path'),
    router = express.Router();

// создаем парсер для данных application/x-www-form-urlencoded
//const urlencodedParser = bodyParser.urlencoded({extended: false});

router.use('/api', require('./api'));
/*router.get("/", (request, response) => {
    response.sendFile(pathToView + 'index.html');
});*/

module.exports = router;