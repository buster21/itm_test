const createError = require('http-errors'),
    express = require('express'),
    app = express(),
    path = require('path'),
    morgan = require('morgan'),
    cors = require('cors');

app.use(cors());
app.use(morgan('combined'));
app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'views')));
app.use('/images',express.static(path.join(__dirname, 'uploads')));
require('./core/services/initDb');
require('./core/services/passport');
app.use('/',require('./routes'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({"error":err.message,"code":err.status});
});

module.exports = app;