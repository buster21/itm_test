const   app = require('../server'),
        config = require('../config'),
        debug = require('debug')('src:server'),
        http = require('http'),
        ioJwt = require('socketio-jwt'),
        push = require('../core/services/randomPush');

app.set("env",config.env);
app.set('port', process.env.PORT || 3000);

server = http.createServer(app);
let io = require('socket.io')(server);
server.listen(app.get("port"));
server.on('error', onError);
server.on('listening', onListening);

let usersInSocket = [];
io.use(ioJwt.authorize({
    secret: config.secretJWT,
    handshake: true
})).on('connection', socket => {
    usersInSocket[ socket.decoded_token.id ] = socket;
});

// Listener Event push-images
app.on('push-images',() => {
    push().then( targets => {
        targets.forEach(t => {
            if( usersInSocket[t.to_user_id] ) usersInSocket[t.to_user_id].emit('new-image', t.image);
        });
    }).catch( e => console.log(e) );
});

/**
* Error event
*/
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string' ? 'Pipe ' + app.get("port") : 'Port ' + app.get("port");

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener
 */

function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}